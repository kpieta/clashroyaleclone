﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MinionWave : ScriptableObject
{
    public List<int> waveCounter;
    public List<GameObject> MinionWaveList;

}
