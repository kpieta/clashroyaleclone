﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class CreateMinionWaveList
{
    [MenuItem("Assets/Create/Minion Wave List")]

    public static MinionWave Create()
    {
        MinionWave asset = ScriptableObject.CreateInstance<MinionWave>();
        AssetDatabase.CreateAsset(asset, "Assets/MinionWave.asset");
        AssetDatabase.SaveAssets();
        return asset;
    }
}
