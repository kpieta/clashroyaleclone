﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class UIShopController : MonoBehaviour
{
    [SerializeField]
    private MinionToBuy minionsToBuy;
    [SerializeField]
    private PlayerMinionButtons playerMinions;
    [SerializeField]
    private Text goldAmmount;
    [SerializeField]
    private Text communicationToPlayerText;

    public void Awake()
    {
        GameEvent.DeckUpdated += GameEvent_DeckUpdated;
        GameEvent.BoughtNewMinion += GameEvent_BoughtNewMinion;
        GameEvent.GoldSpend += GameEvent_GoldSpend;
        GameEvent.SendInfoOnScreen += GameEvent_SendInfoOnScreen;
    }

    public void OnDestroy()
    {
        GameEvent.DeckUpdated -= GameEvent_DeckUpdated;
        GameEvent.BoughtNewMinion -= GameEvent_BoughtNewMinion;
        GameEvent.GoldSpend -= GameEvent_GoldSpend;
        GameEvent.SendInfoOnScreen -= GameEvent_SendInfoOnScreen;
    }

    private void GameEvent_SendInfoOnScreen(string message)
    {
        if (!communicationToPlayerText.gameObject.activeInHierarchy)
        {
            communicationToPlayerText.text = message;
            communicationToPlayerText.gameObject.SetActive(true);
            StartCoroutine(DisplayMessage());
        }
    }

    private IEnumerator DisplayMessage()
    {
        yield return new WaitForSeconds(2);
        communicationToPlayerText.gameObject.SetActive(false);
    }

    public void Start()
    {
        communicationToPlayerText.gameObject.SetActive(false);
        goldAmmount.text = "Gold: " + CardShopManager.PlayerManager.Gold;
    }

    private void GameEvent_GoldSpend(int goldLeft)
    {
        goldAmmount.text = "Gold: " + goldLeft;
    }

    private void GameEvent_BoughtNewMinion()
    {
        minionsToBuy.ButtonUpdate();
        playerMinions.AddMinionButton();
    }


    private void GameEvent_DeckUpdated()
    {
        playerMinions.ButtonUpdate();
    }
}
