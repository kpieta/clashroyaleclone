﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SpawnButtons : MonoBehaviour
{
    

    [SerializeField]
    Button button1;
    [SerializeField]
    Button button2;
    [SerializeField]
    Button button3;
    [SerializeField]
    Button button4;

    Button[] _buttonArray;
    MinionEnum[] _handDeck;
    Image[] _icons;


    public void Awake()
    {
        _buttonArray = new Button[4];
        _buttonArray[0] = button1;
        _buttonArray[1] = button2;
        _buttonArray[2] = button3;
        _buttonArray[3] = button4;
    }

    public void OnButton1Click()
    {
        MinionEnum minionAssignedToButton = _handDeck[0];
        GameEvent.OnMinionSelected(minionAssignedToButton);
    }

    public void OnButton2Click()
    {
        MinionEnum minionAssignedToButton = _handDeck[1];
        GameEvent.OnMinionSelected(minionAssignedToButton);
    }

    public void OnButton3Click()
    {
        MinionEnum minionAssignedToButton = _handDeck[2];
        GameEvent.OnMinionSelected(minionAssignedToButton);
    }

    public void OnButton4Click()
    {
        MinionEnum minionAssignedToButton = _handDeck[3];
        GameEvent.OnMinionSelected(minionAssignedToButton);
    }

    public void RefreshIcons()
    {
        _handDeck = Manager.PlayerBattleManager.GetHandDeck();
        for (int i = 0; i < _buttonArray.Length; i++)
        {
            Sprite sprite = Resources.Load<Sprite>("Icons/" + _handDeck[i]);
            _buttonArray[i].image.sprite = sprite;
        }
    }
}
