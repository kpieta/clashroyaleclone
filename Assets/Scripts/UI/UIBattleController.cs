﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class UIBattleController : MonoBehaviour
{
    [SerializeField]
    private SpawnButtons spawnButtons;
    [SerializeField]
    private ManaBar manaBar;
    [SerializeField]
    private Text textField;

    public void Awake()
    {
        GameEvent.MinionSpawned += GameEvent_MinionSpawned;
        GameEvent.DeckUpdated += GameEvent_DeckUpdated;
        GameEvent.SendInfoOnScreen += GameEvent_SendInfoOnScreen;
        GameEvent.ManaUpdated += GameEvent_ManaUpdated;
    }

    private void GameEvent_ManaUpdated(int manaAmmount)
    {
        manaBar.RefreshBar(manaAmmount);
    }

    private void GameEvent_SendInfoOnScreen(string message)
    {
        if (textField.text != message)
        {
            StartCoroutine(ShowMessage(message));
        }
    }

    private IEnumerator ShowMessage(string message)
    {
        textField.text = message;
        yield return new WaitForSeconds(1.0f);
        textField.text = "";
    }

    public void OnDestroy()
    {
        GameEvent.MinionSpawned -= GameEvent_MinionSpawned;
        GameEvent.DeckUpdated -= GameEvent_DeckUpdated;
        GameEvent.SendInfoOnScreen -= GameEvent_SendInfoOnScreen;
        GameEvent.ManaUpdated -= GameEvent_ManaUpdated;
    }

    private void GameEvent_MinionSpawned(MinionEnum minion)
    {
        Debug.Log("UIController method event handler to complete");
    }

    private void GameEvent_DeckUpdated()
    {
        spawnButtons.RefreshIcons();
    }

}
