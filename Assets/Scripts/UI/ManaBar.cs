﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class ManaBar : MonoBehaviour
{

    private Slider _slider;

    public void Awake()
    {
        _slider = GetComponent<Slider>();
    }

    public void RefreshBar(int manaAmmount)
    {
        _slider.value += manaAmmount;
    }
}
