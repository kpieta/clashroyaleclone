﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class SpawnerManager : MonoBehaviour, IGameManager
{

    public List<GameObject> PlayersSpawnPoints;
    public List<GameObject> AISpawnPoints;

    [SerializeField]
    private MinionFactory minionFactory;
    private MinionEnum _selectedMinion;
    private List<Animator> _spawnPointsAnimatorList;

    public void Startup()
    {
        GameEvent.MinionSelected += GameEvent_MinionSelected;
        _spawnPointsAnimatorList = new List<Animator>();
        foreach (GameObject spawnPoint in PlayersSpawnPoints)
        {
            Animator animator = spawnPoint.GetComponent<Animator>();
            _spawnPointsAnimatorList.Add(animator);
            spawnPoint.SetActive(false);
        }
        //minionFactory = GetComponent<MinionFactory>();
    }

    private void GameEvent_MinionSelected(MinionEnum minion)
    {
        _selectedMinion = minion;
        foreach (GameObject spawnWayPoint in PlayersSpawnPoints)
        {
            spawnWayPoint.SetActive(true);
        }
    }

    public void OnDestroy()
    {
        GameEvent.MinionSelected -= GameEvent_MinionSelected;
    }

    public void SpawnSelectedMinion(Transform spawnPoint)
    {
        if (_selectedMinion != MinionEnum.None)
        {
            GameObject minionToSpawn = minionFactory.GetMinion(_selectedMinion, Requestor.Player);

            if (minionToSpawn != null)
            {
                if (minionToSpawn.GetComponent<MinionParameters>().ManaCost > Manager.PlayerBattleManager.CurrentMana)
                {
                    GameEvent.OnSendInfoOnScreen("Not enough mana");
                    return;
                }
                minionToSpawn.transform.position = new Vector3(spawnPoint.position.x, spawnPoint.position.y, 0);
                minionToSpawn.transform.rotation = spawnPoint.rotation;
                minionToSpawn.SetActive(true);
                Manager.PlayerBattleManager.RemoveMinion(minionToSpawn.GetComponent<MinionParameters>().MinionName);
                Manager.PlayerBattleManager.UseMana(minionToSpawn.GetComponent<MinionParameters>().ManaCost);
                _selectedMinion = MinionEnum.None;
            }
            foreach (GameObject spawnWayPoint in PlayersSpawnPoints)
            {
                spawnWayPoint.SetActive(false);
            }
        }
    }
}
