﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class PlayerBattleManager : MonoBehaviour, IGameManager
{

    public int StartingMana;
    public float CurrentMana { get; private set; }
    public int MaxMana { get { return 10; } private set { } }
    public float ManaRegenCooldown = 3.0f;


    private MinionEnum[] _handDeck;
    private MinionEnum[] _battleDeck;

    public void Startup()
    {
        _handDeck = new MinionEnum[4];
        _battleDeck = Manager.PlayerManager.GetMinionDeck();
        CurrentMana = StartingMana;
        GameEvent.OnManaUpdated(StartingMana);
        PrepareStartingHand();
        InvokeRepeating("ManaRegen", ManaRegenCooldown, ManaRegenCooldown);
    }

    public void UseMana(int ammount)
    {
        if (CurrentMana >= ammount)
        {
            CurrentMana -= ammount;
            GameEvent.OnManaUpdated(-ammount);
        }
    }

    private void ManaRegen()
    {
        if (CurrentMana < MaxMana)
        {
            CurrentMana++;
            GameEvent.OnManaUpdated(1);
        }
        else
        {
            CurrentMana = MaxMana;
        }
    }

    private void PrepareStartingHand()
    {
        List<MinionEnum> tempList = new List<MinionEnum>(_battleDeck);
        for (int i = 0; i < _handDeck.Length; i++)
        {
            int randomIndex = UnityEngine.Random.Range(0, tempList.Count);
            _handDeck[i] = tempList[randomIndex];
            tempList.RemoveAt(randomIndex);
        }
        GameEvent.OnHandDeckUpdated();
    }

    public void RemoveMinion(MinionEnum minion)
    {
        int minionToReplaceIndex = Array.FindIndex<MinionEnum>(_handDeck, minionToFind => minionToFind.Equals(minion));
        ReplaceMinion(minionToReplaceIndex);
    }

    void ReplaceMinion(int indexToReplace)
    {
        int randomIndex = UnityEngine.Random.Range(0, _battleDeck.Length);
        MinionEnum newMinion = _battleDeck[randomIndex];
        bool exists = Array.Exists<MinionEnum>(_handDeck, x => x.Equals(newMinion));
        if (!exists)
        {
            _handDeck[indexToReplace] = newMinion;
            GameEvent.OnHandDeckUpdated();
            return;
        }
        ReplaceMinion(indexToReplace);
    }

    public MinionEnum[] GetHandDeck()
    {
        return _handDeck;
    }

}
