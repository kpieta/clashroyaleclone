﻿using UnityEngine;
using System.Collections;
using System;

public abstract class BattleManager : MonoBehaviour, IGameManager
{
    public float CurrentMana { get; set; }
    public int MaxMana { get { return 10; } private set { } }
    public float ManaRegen { get { return .1f; } private set { } }

    public void UseMana(int ammount)
    {
        if (CurrentMana >= ammount)
        {
            CurrentMana = Mathf.FloorToInt(CurrentMana - ammount);
        }
    }

    public abstract void Startup();

    public abstract void RefillMana();



}
