﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

[DataContract(Name = "PlayerManager", Namespace = "")]
public class PlayerManager : MonoBehaviour, IGameManager
{

    [DataMember]
    public int Gold { get; private set; }
    public List<MinionEnum> MinionInPossess { get; private set; }

    [SerializeField]
    private int initialGold;
    [DataMember(Name = "MinionCollections")]
    private MinionEnum[] _deckMinions;
    public MinionEnum[] GetMinionDeck()
    {
        return _deckMinions;
    }

    public void Startup()
    {
        MinionInPossess = new List<MinionEnum>();
        _deckMinions = new MinionEnum[5];
    }

    public void SetUpLoadeData(DataToBeSaved playersData)
    {
        DataToBeSaved loadedData = playersData;
        if (loadedData.IsDataReadable())
        {
            Gold = loadedData.GetGold();
            MinionInPossess = loadedData.GetMinionsLists(MinionCollectionType.MinionInPossess) as List<MinionEnum>;
            List<MinionEnum> tempList = loadedData.GetMinionsLists(MinionCollectionType.MinionInDeck) as List<MinionEnum>;
            _deckMinions = tempList.ToArray();
        }
    }

    public void SetupStartingData()
    {
        AddStartingMinions();
        CreateDefaultDeck();
        Gold = initialGold;
    }

    public void AddMinionToDeck(MinionEnum newMinion)
    {
        for (int i = _deckMinions.Length - 1; i >= 1; i--)
        {
            _deckMinions[i] = _deckMinions[i - 1];
        }
        _deckMinions[0] = newMinion;
        GameEvent.OnHandDeckUpdated();
    }

    private void AddStartingMinions()
    {
        MinionInPossess.Add(MinionEnum.Skeleton);
        MinionInPossess.Add(MinionEnum.Spider);
        MinionInPossess.Add(MinionEnum.RedSpider);
        MinionInPossess.Add(MinionEnum.RedSkeleton);
        MinionInPossess.Add(MinionEnum.BlueSkeleton);
    }

    private void CreateDefaultDeck()
    {
        for (int i = 0; i < _deckMinions.Length; i++)
        {
            _deckMinions[i] = MinionInPossess[i];
        }
    }

    public void UpdateGold(int ammount, GoldActivities goldActivities)
    {
        if (goldActivities == GoldActivities.Earning)
        {
            Gold += ammount;
        }
        else
        {
            Gold -= ammount;
            GameEvent.OnGoldSpend(Gold);
        }
    }

    public void AddMinion(MinionEnum minion)
    {
        MinionInPossess.Add(minion);
        Debug.Log("Minion dodany " + minion);
        GameEvent.OnBouthtNewMinion();
    }
}
