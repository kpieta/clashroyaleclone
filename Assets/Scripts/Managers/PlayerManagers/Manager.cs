﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization;

[DataContract(Name = "Manager", Namespace = "")]
public class Manager : MonoBehaviour
{
    public static WayPointManager Waypoints { get; private set; }
    [DataMember]
    public static PlayerManager PlayerManager { get; private set; }
    public static PlayerBattleManager PlayerBattleManager { get; private set; }
    public static SpawnerManager SpawnerManager{ get; private set; }
    public static GameManager GameManager { get; private set; }

    private List<IGameManager> managers;

    void Awake()
    {
        Waypoints = GetComponent<WayPointManager>();
        PlayerManager = GetComponent<PlayerManager>();
        PlayerBattleManager = GetComponent<PlayerBattleManager>();
        SpawnerManager = GetComponent<SpawnerManager>();
        GameManager = GetComponent<GameManager>();

        managers = new List<IGameManager>();
        managers.Add(Waypoints);
        managers.Add(SpawnerManager);
        managers.Add(PlayerManager);
        managers.Add(GameManager);
        managers.Add(PlayerBattleManager);

        StartAllManagers();
    }

    private void StartAllManagers()
    {
        foreach (IGameManager manager in managers)
        {
            manager.Startup();
        }
    }
}
