﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class WayPointManager : MonoBehaviour, IGameManager
{

    public Transform LeftWaypointsParent;
    public Transform RightWayPointsParent;

    public StartingPosition startingPosition;

    private List<Transform> leftWaypoints;
    private List<Transform> rightWayPoints;

    WaypointComparer comparer = new WaypointComparer();

    public void Startup()
    {
        leftWaypoints = new List<Transform>();
        rightWayPoints = new List<Transform>();

        foreach (Transform waypoint in LeftWaypointsParent.transform)
        {
            leftWaypoints.Add(waypoint);
        }

        foreach (Transform waypoint in RightWayPointsParent.transform)
        {
            rightWayPoints.Add(waypoint);
        }

        SortLists();
    }

    private void SortLists()
    {
        comparer.SetStartingPosition(startingPosition);
        comparer.SetPathPosition(Path.Left);
        leftWaypoints.Sort(comparer);
        comparer.SetPathPosition(Path.Right);
        rightWayPoints.Sort(comparer);
    }

    public List<Transform> GetWayPointsList(bool isLeft)
    {
        if (isLeft)
        {
            return leftWaypoints;
        }
        else
        {
            return rightWayPoints;
        }
    }

}
