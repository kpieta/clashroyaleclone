﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using UnityEngine.EventSystems;

public class PlayerMinionButtons : MonoBehaviour
{
    public int MaxButtonsInRow = 5;
    public float xOffset = 80;
    public float yOffset = -100;

    private List<Button> _buttonList;
    [SerializeField]
    private Button originalButton;


    public void Awake()
    {
        originalButton.gameObject.SetActive(true);
        _buttonList = new List<Button>();
    }

    public void Start()
    {
        //TODO TUTAJ! trzeba zobaczyc co sie dzieje przy tworzeniu guzikow na samym starcie i poprawic tak aby byly guziki po rozpoczeciu lvlu.
        setButtons();
        setupEvents();
    }

    public void AddMinionButton()
    {
        originalButton.gameObject.SetActive(true);
        Button newButton = Instantiate(originalButton);
        int indexOfRecentAddedMinion = CardShopManager.PlayerManager.MinionInPossess.Count - 1;
        newButton.GetComponent<IMinionButtons>().AttachMinionToButton(CardShopManager.PlayerManager.MinionInPossess[indexOfRecentAddedMinion]);
        newButton.transform.SetParent(transform, false);
        newButton.transform.localScale = Vector3.one;
        int indexOfLastButton = _buttonList.Count - 1;
        newButton.GetComponent<RectTransform>().anchoredPosition = new Vector2(_buttonList[indexOfLastButton].GetComponent<RectTransform>().anchoredPosition.x + xOffset, _buttonList[indexOfLastButton].GetComponent<RectTransform>().anchoredPosition.y);
        _buttonList.Add(newButton);
        originalButton.gameObject.SetActive(true);
        setupEvents();
    }

    private void setButtons()
    {
        int buttonCounter = 0;
        int rowCounter = 0;
        foreach (MinionEnum minion in CardShopManager.PlayerManager.MinionInPossess)
        {
            if (buttonCounter > MaxButtonsInRow)
            {
                buttonCounter = 0;
                rowCounter++;
            }
            Button newButton = Instantiate(originalButton);
            newButton.GetComponent<IMinionButtons>().AttachMinionToButton(minion);
            newButton.transform.SetParent(transform, false);
            newButton.transform.localScale = Vector3.one;
            newButton.GetComponent<RectTransform>().anchoredPosition = new Vector2(originalButton.GetComponent<RectTransform>().anchoredPosition.x + xOffset * buttonCounter, originalButton.GetComponent<RectTransform>().anchoredPosition.x + yOffset * rowCounter);
            _buttonList.Add(newButton);
            buttonCounter++;
        }
        originalButton.gameObject.SetActive(false);
    }

    private void setupEvents()
    {
        UIService.GetIntance().SetupEventsForButtonCollection(_buttonList, CardShopManager.PlayerManager.GetMinionDeck(), onButtonClick);
    }

    internal void ButtonUpdate()
    {
        foreach (Button button in _buttonList)
        {
            button.GetComponent<DeckButtons>().RefreshIcon();
        }
        setupEvents();
    }

    private void onButtonClick(IMinionButtons button)
    {
        CardShopManager.PlayerManager.AddMinionToDeck(button.Minion);
    }
}
