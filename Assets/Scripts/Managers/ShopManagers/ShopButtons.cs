﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;

public class ShopButtons : MonoBehaviour, IMinionButtons
{
    public MinionEnum Minion
    {
        get
        {
            return _minion;
        }
    }

    public int Cost { get; private set; }

    bool canBeBought;

    private Button _button;
    private MinionEnum _minion;
    public void Awake()
    {
        _button = GetComponent<Button>();
        canBeBought = true;
    }

    public void RefreshIcon()
    {
        canBeBought = !CardShopManager.PlayerManager.MinionInPossess.Contains(Minion);
        if (!canBeBought)
        {
            _button.image.color = Color.black;
        }
    }

    public void SetMinionCost(int value)
    {
        Cost = value;
    }

    public void AttachMinionToButton(MinionEnum minion)
    {
        _minion = minion;
        UIService.GetIntance().GetIconForButton(_button, minion);
        RefreshIcon();
    }
}
