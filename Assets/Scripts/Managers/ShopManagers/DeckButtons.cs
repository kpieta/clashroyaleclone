﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public class DeckButtons : MonoBehaviour, IMinionButtons
{

    public MinionEnum Minion
    {
        get
        {
            return _minion;
        }
    }

    private MinionEnum _minion;
    private Button _button;

    // Use this for initialization
    void Awake()
    {
        _button = GetComponent<Button>();
    }

    public void AttachMinionToButton(MinionEnum minion)
    {
        _minion = minion;
        UIService.GetIntance().GetIconForButton(_button, minion);
        RefreshIcon();
    }

    public void RefreshIcon()
    {
        List<MinionEnum> playersDeck = new List<MinionEnum>(CardShopManager.PlayerManager.GetMinionDeck());
        bool isInTheDeck = playersDeck.Contains(_minion);
        if (isInTheDeck)
        {
            _button.image.color = Color.black;
        }
        else
        {
            _button.image.color = Color.white;
        }
    }

}
