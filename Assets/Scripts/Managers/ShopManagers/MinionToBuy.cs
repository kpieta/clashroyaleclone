﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MinionToBuy : MonoBehaviour
{

    public int ButtonsInRow = 5;
    public float xOffset = 80;
    public float yOffset = -100;

    private List<MinionEnum> _totalMinionList;
    private List<Button> _buttonList;
    [SerializeField]
    private Button originalButton;

    public void Awake()
    {
        _totalMinionList = new List<MinionEnum>();
        _buttonList = new List<Button>();
        originalButton.gameObject.SetActive(true);
    }

    public void Start()
    {
        loadAllMinionsToList();
        setButtons();
        setupEvents();
    }

    private void loadAllMinionsToList()
    {
        foreach (MinionEnum minion in Enum.GetValues(typeof(MinionEnum)))
        {
            if (minion != MinionEnum.None)
            {
                _totalMinionList.Add(minion);
            }
        }
    }

    private void setButtons()
    {
        int buttonCounter = 0;
        int rowCounter = 0;
        foreach (MinionEnum minion in _totalMinionList)
        {
            if (buttonCounter > ButtonsInRow)
            {
                buttonCounter = 0;
                rowCounter++;
            }
            Button newButton = Instantiate(originalButton);
            Text cost = newButton.GetComponentInChildren<Text>();

            newButton.GetComponent<IMinionButtons>().AttachMinionToButton(minion);
            newButton.transform.SetParent(transform, false);
            newButton.transform.localScale = Vector3.one;
            newButton.GetComponent<RectTransform>().anchoredPosition = new Vector2(originalButton.GetComponent<RectTransform>().anchoredPosition.x + xOffset * buttonCounter, originalButton.GetComponent<RectTransform>().anchoredPosition.x + yOffset * rowCounter);
            if (newButton.GetComponent<IMinionButtons>() is ShopButtons)
            {
                ShopButtons shopButton = newButton.GetComponent<IMinionButtons>() as ShopButtons;
                shopButton.SetMinionCost(10 + buttonCounter * 10);
                cost.text = shopButton.Cost + " Gold";
            }
            _buttonList.Add(newButton);
            buttonCounter++;
        }
        originalButton.gameObject.SetActive(false);
    }

    private void setupEvents()
    {
        UIService.GetIntance().SetupEventsForButtonCollection(_buttonList, CardShopManager.PlayerManager.MinionInPossess, onButtonClick);
    }


    private void onButtonClick(IMinionButtons minionButton)
    {
        if (minionButton is ShopButtons)
        {
            ShopButtons shopButton = minionButton as ShopButtons;
            if (CardShopManager.PlayerManager.Gold >= shopButton.Cost)
            {
                CardShopManager.PlayerManager.UpdateGold(shopButton.Cost, GoldActivities.Spending);
                CardShopManager.PlayerManager.AddMinion(shopButton.Minion);
            }
            else
            {
                GameEvent.OnSendInfoOnScreen("Not enough Gold");
            }
        }
    }

    public void ButtonUpdate()
    {
        Debug.Log("Updating Buttons in MinionToBuy Class");
        foreach (Button button in _buttonList)
        {
            button.GetComponent<ShopButtons>().RefreshIcon();
        }
        setupEvents();
    }
}
