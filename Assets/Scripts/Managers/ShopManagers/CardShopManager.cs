﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class CardShopManager : MonoBehaviour
{
    public static PlayerManager PlayerManager { get; private set; }
    public static GameManager GameManager { get; private set; }

    private List<IGameManager> managers;

    public void Awake()
    {

        PlayerManager = GetComponent<PlayerManager>();
        GameManager = GetComponent<GameManager>();

        managers = new List<IGameManager>();

        managers.Add(PlayerManager);
        managers.Add(GameManager);
        StartAllManagers();

    }

    private void StartAllManagers()
    {
        foreach (IGameManager manager in managers)
        {
            manager.Startup();
        }
    }
}
