﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(WayPointManager))]
[RequireComponent(typeof(AIBattleManager))]

public class AIManager : MonoBehaviour
{
    public static WayPointManager AIWaypoints { get; private set; }
    public static AIBattleManager AIBattleManager { get; private set; }

    private List<IGameManager> gameManagers;

    void Awake()
    {
        AIWaypoints = GetComponent<WayPointManager>();
        AIBattleManager = GetComponent<AIBattleManager>();

        gameManagers = new List<IGameManager>();
        gameManagers.Add(AIWaypoints);
        gameManagers.Add(AIBattleManager);

        StartAllManagers();
    }

    private void StartAllManagers()
    {
        foreach (IGameManager manager in gameManagers)
        {
            manager.Startup();
        }
    }
}
