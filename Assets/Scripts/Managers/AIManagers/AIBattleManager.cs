﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class AIBattleManager : MonoBehaviour, IGameManager
{
    public float spawnTime = 1f;
    [Range(0, 10)]
    public float spawnDelta = 5f;
    public Transform[] SpawnPoints;
    public List<int> Counter;
    public List<GameObject> MinionWave;
    [SerializeField]
    private MinionFactory minionFactory;
    private float _activatingMinionDelta = .5f;

    public void Startup()
    {
        InvokeRepeating("SpawnMinions", spawnTime, _activatingMinionDelta);
    }

    void SpawnMinions()
    {
        if (MinionWave.Count == 0)
        {
            return;
        }
        int waypointNumber = UnityEngine.Random.Range(0, 2);
        if (Counter[0] <= 0 && Counter.Count != 0)
        {
            Counter.RemoveAt(0);
            StartCoroutine(WaitForNextWave(spawnDelta));
            return;
        }

        GameObject minionToSpawn = minionFactory.GetMinion(MinionWave[0].GetComponent<MinionParameters>().MinionName, Requestor.AI);
        if (minionToSpawn != null)
        {
            minionToSpawn.transform.position = SpawnPoints[waypointNumber].position;
            minionToSpawn.transform.rotation = SpawnPoints[waypointNumber].rotation;
            minionToSpawn.SetActive(true);
            MinionWave.RemoveAt(0);
            Counter[0] -= 1;
        }
    }

    private IEnumerator WaitForNextWave(float seconds)
    {
        CancelInvoke("SpawnMinions");
        yield return new WaitForSeconds(seconds);
        InvokeRepeating("SpawnMinions", spawnTime, _activatingMinionDelta);
    }
}
