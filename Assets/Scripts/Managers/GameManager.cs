﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;
using System.IO;

public class GameManager : MonoBehaviour, IGameManager
{

    private int _goldAccumulated;
    private int _level;
    string path;
    private const string PLAYERS_DATA_FILE_NAME = "PlayersData";
    private DataToBeSaved savedData;

    public void Startup()
    {

        CreateSaveDirectory();
        savedData = new DataToBeSaved();
        _level = PlayerPrefs.GetInt("CurrentLevel");
        if (Application.loadedLevelName == "Level1")
        {
            Manager.PlayerManager.SetupStartingData();
        }
        else
        {
            LoadPlayersData();
        }
        GameEvent.TowerDestroyed += GameEvent_TowerDestroyed;
    }

    private void LoadPlayersData()
    {
        string dataPath = path + "/" + PLAYERS_DATA_FILE_NAME;
        Debug.Log(dataPath);
        if (File.Exists(path + "/" + PLAYERS_DATA_FILE_NAME))
        {
            DataToBeSaved loadedData = DataService.GetInstance().LoadData<DataToBeSaved>(path, PLAYERS_DATA_FILE_NAME, "SavedData");
            if (Application.loadedLevelName == "Shop")
            {
                CardShopManager.PlayerManager.SetUpLoadeData(loadedData);
            }
            else
            {
                Manager.PlayerManager.SetUpLoadeData(loadedData);
            }
        }
    }

    private void SavePlayersData()
    {
        savedData.WriteParametersToBeSaved(Manager.PlayerManager.Gold, Manager.PlayerManager.MinionInPossess, Manager.PlayerManager.GetMinionDeck());
        DataService.GetInstance().SaveData(path, PLAYERS_DATA_FILE_NAME, savedData, "SavedData");
    }

    private void SavePlayersShopping()
    {
        savedData.WriteParametersToBeSaved(CardShopManager.PlayerManager.Gold, CardShopManager.PlayerManager.MinionInPossess, CardShopManager.PlayerManager.GetMinionDeck());
        DataService.GetInstance().SaveData(path, PLAYERS_DATA_FILE_NAME, savedData, "SavedData");
    }

    private void CreateSaveDirectory()
    {
        path = Application.dataPath + "/" + "Save";
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
    }

    private void GameEvent_TowerDestroyed(GameObject sender)
    {
        if (sender.tag == "AIBuilding")
        {
            Manager.PlayerManager.UpdateGold(_goldAccumulated, GoldActivities.Earning);
            _level++;
            SavePlayersData();
            SceneManager.LoadScene("Shop");
        }
        else
        {
            SavePlayersData();
            SceneManager.LoadScene("Shop");
        }
    }

    public void OnDestroy()
    {
        GameEvent.TowerDestroyed -= GameEvent_TowerDestroyed;
        PlayerPrefs.SetInt("CurrentLevel", _level);
    }

    public void GatherGold(int ammount)
    {
        _goldAccumulated += ammount;
    }

    public void LoadNextLevel()
    {
        SavePlayersShopping();
        SceneManager.LoadScene("Level" + _level);
    }

}
