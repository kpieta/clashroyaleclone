﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization;
using System.IO;
using System.Xml;

public class DataService
{
    private static DataService instance;

    private DataService()
    {

    }

    public static DataService GetInstance()
    {
        if (instance == null)
        {
            instance = new DataService();
        }
        return instance;
    }

    public void SaveData<T>(string directory, string fileName, T objectToSave, string rootName)
    {
        DataContractSerializer serializer = new DataContractSerializer(typeof(T), rootName, "");
        using (FileStream output = new FileStream(directory + "\\" + fileName, FileMode.Create))
        {
            serializer.WriteObject(output, objectToSave);
        }
    }

    public T LoadData<T>(string directory, string fileName, string rootName)
    {
        DataContractSerializer serializer = new DataContractSerializer(typeof(T), rootName, "");
        using (FileStream input = new FileStream(directory + "\\" + fileName, FileMode.Open))
        using (XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(input, new XmlDictionaryReaderQuotas()))
        {
            T objectRead = (T)serializer.ReadObject(reader, true);
            return objectRead;
        }
    }
}
