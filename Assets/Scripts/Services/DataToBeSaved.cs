﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

[DataContract(Name = "DataToBeSaved", Namespace = "")]
public class DataToBeSaved
{
    [DataMember]
    private int Gold;
    [DataMember]
    private Dictionary<MinionCollectionType, IList<MinionEnum>> minionsMap;

    public DataToBeSaved()
    {
        Gold = 0;
        minionsMap = new Dictionary<MinionCollectionType, IList<MinionEnum>>();
    }

    public void WriteParametersToBeSaved(int gold, IList<MinionEnum> minionPossessed, IList<MinionEnum> minionInDeck)
    {
        Gold = gold;
        minionsMap.Clear();
        minionsMap.Add(MinionCollectionType.MinionInPossess, minionPossessed);
        minionsMap.Add(MinionCollectionType.MinionInDeck,minionInDeck);
    }

    public bool IsDataReadable()
    {
        foreach (KeyValuePair<MinionCollectionType,IList<MinionEnum>> minionList in minionsMap)
        {
            if (minionList.Value.Count == 0)
            {
                return false;
            }
        }
        return true;
    }

    public int GetGold()
    {
        return Gold;
    }

    public IList<MinionEnum> GetMinionsLists(MinionCollectionType collectionTypeToRetrieve)
    {
        return minionsMap[collectionTypeToRetrieve];
    }




}
