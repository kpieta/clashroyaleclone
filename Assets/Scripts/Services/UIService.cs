﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;

public class UIService
{
    private static UIService instance;

    private UIService()
    {

    }

    public static UIService GetIntance()
    {
        if (instance == null)
        {
            instance = new UIService();
        }

        return instance;
    }

    public void GetIconForButton(Button button, MinionEnum minionAttachedToButton)
    {
        Sprite sprite = Resources.Load<Sprite>("Icons/" + minionAttachedToButton);
        button.image.sprite = sprite;
    }

    public void SetupEventsForButtonCollection(IEnumerable buttonList, IList<MinionEnum> listToCompare, Action<IMinionButtons> eventhandler)
    {
        foreach (Button button in buttonList)
        {
            IMinionButtons minionButton = button.GetComponent<IMinionButtons>();
            EventTrigger trigger = button.GetComponent<EventTrigger>();
            trigger.triggers.Clear();

            if (!listToCompare.Contains(minionButton.Minion))
            {
                EventTrigger.Entry entry = new EventTrigger.Entry();
                entry.eventID = EventTriggerType.PointerClick;
                entry.callback.AddListener((BaseEventData data) => { eventhandler(minionButton); });
                trigger.triggers.Add(entry);
            }
        }
    }

}
