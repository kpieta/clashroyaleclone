﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization;

[DataContract(Name = "MinionEnum", Namespace = "")]
public enum MinionEnum
{
    [EnumMember(Value = "None")]
    None,
    [EnumMember(Value = "Skeleton")]
    Skeleton,
    [EnumMember(Value = "RedSkeleton")]
    RedSkeleton,
    [EnumMember(Value = "BlueSkeleton")]
    BlueSkeleton,
    [EnumMember(Value = "GreenSkeleton")]
    GreenSkeleton,
    [EnumMember(Value = "Spider")]
    Spider,
    [EnumMember(Value = "RedSpider")]
    RedSpider,
    [EnumMember(Value = "BlueSpider")]
    BlueSpider,
    [EnumMember(Value = "GreenSpider")]
    GreenSpider,

}

public enum GoldActivities
{
    Earning,
    Spending,
}

public enum MinionCollectionType
{
    MinionInPossess,
    MinionInDeck,
}