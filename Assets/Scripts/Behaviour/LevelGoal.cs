﻿using UnityEngine;
using System.Collections;

public class LevelGoal : MonoBehaviour
{
    public void OnDestroy()
    {
        GameEvent.OnTowerDestroyed(gameObject);
    }
}
