﻿using UnityEngine;
using System.Collections;

public class SpawningPoints : MonoBehaviour
{
    public void OnMouseDown()
    {
        Manager.SpawnerManager.SpawnSelectedMinion(gameObject.transform);
    }
}
