﻿using UnityEngine;
using System.Collections;

public class LookingForEnemy : MonoBehaviour
{

    private Movement _movement;

    void Start()
    {

        Movement movementComponent = GetComponentInParent<Movement>();
        if (movementComponent is PlayerMovement)
        {
            _movement = movementComponent as PlayerMovement;
        }
        else if (movementComponent is AIMovement)
        {
            _movement = movementComponent as AIMovement;
        }
        //tag = GetComponentInParent<Component>().tag;
    }

    void Update()
    {
        if (gameObject.activeSelf)
        {
            if (_movement.MovementTarget != null)
            {

                Vector2 direction = _movement.MovementTarget.position - transform.position;
                RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, GetComponentInParent<MinionParameters>().SightRange);
                if (hit.rigidbody != null && _movement is PlayerMovement)
                {
                    if (hit.rigidbody.gameObject.tag == "AI" || hit.rigidbody.gameObject.tag == "AIBuilding")
                    {
                        _movement.SetEnemyPosition(hit.transform);
                        _movement.IsEnemyInSight(true);
                        return;
                    }
                }
                else if (hit.rigidbody != null && _movement is AIMovement)
                {
                    if (hit.rigidbody.tag == "Player" || hit.rigidbody.tag == "PlayerBuilding")
                    {
                        _movement.SetEnemyPosition(hit.transform);
                        _movement.IsEnemyInSight(true);
                        return;
                    }
                }
            }
            _movement.IsEnemyInSight(false);
            _movement.SetEnemyPosition(null);
        }
    }
}

//if (gameObject.activeSelf)
//        {
//            if (_movement.MovementTarget != null)
//            {
//                Vector2 direction = _movement.MovementTarget.position - transform.position;
//RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, GetComponentInParent<MinionParameters>().SightRange);
//                if (hit.rigidbody != null)
//                {
//                    if (tag == "Player" && hit.rigidbody.gameObject.tag == "AI" || hit.rigidbody.gameObject.tag == "AIBuilding")
//                    {
//                        _movement.SetEnemyPosition(hit.transform);
//                        _movement.IsEnemyInSight(true);
//                    }
//                    else if (tag == "AI" && hit.rigidbody.tag == "Player" || hit.rigidbody.tag == "PlayerBuilding")
//                    {
//                        _movement.SetEnemyPosition(hit.transform);
//                        _movement.IsEnemyInSight(true);
//                    }

//                }
//                else
//                {
//                    _movement.IsEnemyInSight(false);
//                    _movement.SetEnemyPosition(null);
//                }
//            }
//        }
