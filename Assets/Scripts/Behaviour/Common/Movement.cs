﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public abstract class Movement : MonoBehaviour
{

    public Transform EnemyPosition { get; private set; }
    public Transform MovementTarget { get { return movementTarget; } }

    protected bool _enemyInSight;
    protected List<Transform> _waypoints;
    protected Transform movementTarget;
    protected Rigidbody2D _rb2d;

    private float _minDistance = 0.8f;
    private float _pushForce;
    private float _maxSpeed;
    private float _rotationSpeed;

    public void Awake()
    {
        _pushForce = GetComponent<MinionParameters>().PushForce;
        _maxSpeed = GetComponent<MinionParameters>().MovementSpeed;
        _rotationSpeed = GetComponent<MinionParameters>().RotationSpeed;
        _rb2d = GetComponent<Rigidbody2D>();
    }

    public void FixedUpdate()
    {
        if (!_enemyInSight)
        {
            movementTarget = selectWaypoint();
        }
        else
        {
            movementTarget = EnemyPosition;
        }
        rotateTowardWaypoint();
        moveTowardWaypoint();
    }

    private void rotateTowardWaypoint()
    {
        float z = -Mathf.Atan2(movementTarget.transform.position.x - transform.position.x, movementTarget.transform.position.y - transform.position.y) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.Euler(0, 0, z);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, _rotationSpeed);
    }

    private void moveTowardWaypoint()
    {
        Vector3 speed = Vector3.ClampMagnitude(movementTarget.transform.position - transform.position, _maxSpeed);
        speed = Vector3.Normalize(speed);
        speed = speed * _maxSpeed * Time.deltaTime;
        _rb2d.MovePosition(transform.position + speed);
    }

    protected Transform selectWaypoint()
    {
        Transform waypointToReturn = _waypoints[0];

        if (Vector3.Distance(waypointToReturn.transform.position, transform.position) < _minDistance)
        {
            if (_waypoints.Count != 1)
            {
                _waypoints.Remove(waypointToReturn);
            }
        }
        return waypointToReturn;
    }

    public void IsEnemyInSight(bool isInSight)
    {
        _enemyInSight = isInSight;
    }

    public void SetEnemyPosition(Transform enemyPosition)
    {
        EnemyPosition = enemyPosition;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == gameObject.tag && movementTarget != null)
        {
            Vector3 direction = collision.transform.position - transform.position;
            Vector3 referenceVector = movementTarget.position - collision.transform.position;
            if (Vector3.Dot(direction, referenceVector) > 0.90)
            {
                Vector2 forceApplied = new Vector2(-collision.transform.position.x, -collision.transform.position.y);
                forceApplied *= _pushForce;
                Rigidbody2D collisionRigidBody = collision.gameObject.GetComponent<Rigidbody2D>();
                collisionRigidBody.AddForce(forceApplied, ForceMode2D.Force);
            }
        }
    }

    public void EnableMoving(bool enable)
    {
        enabled = enable;
    }

}
