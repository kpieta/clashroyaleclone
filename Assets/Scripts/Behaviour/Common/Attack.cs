﻿using UnityEngine;
using System.Collections;
using System;

public class Attack : MonoBehaviour
{

    private GameObject _targetToAttack;
    private Movement _movement;
    private Animator _animator;
    private Collider2D _collider;
    private bool isAttacking;
    private float _attackRagne;
    private int _damage;
    private int _attackSpeed;

    private int _attackTriggerHash = Animator.StringToHash("Attack");
    private int _attackSpeedHash = Animator.StringToHash("AttackSpeed");
    // Use this for initialization
    void Start()
    {
        isAttacking = false;
        _movement = GetComponent<Movement>();
        _animator = GetComponent<Animator>();
        _collider = GetComponent<Collider2D>();
        _attackRagne = GetComponent<MinionParameters>().AttackRange;
        _damage = GetComponent<MinionParameters>().Damage;
        _attackSpeed = GetComponent<MinionParameters>().AttackSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        AnimatorStateInfo stateInfo = _animator.GetCurrentAnimatorStateInfo(0);

        if (isEnemyPossibleToAttack())
        {
            _movement.EnableMoving(false);
            if (!isAttacking)
            {
                _targetToAttack.GetComponent<IHealth>().DoDamage(_damage);
                isAttacking = true;
                StartCoroutine(WaitForAnimationToEnd(stateInfo));
            }
        }
        else
        {
            if (stateInfo.normalizedTime > 0.94)
            {
                _movement.EnableMoving(true);
            }
        }
    }

    private IEnumerator WaitForAnimationToEnd(AnimatorStateInfo stateInfo)
    {
        _animator.SetFloat(_attackSpeedHash, _attackSpeed);
        _animator.SetTrigger(_attackTriggerHash);
        yield return new WaitForSeconds(stateInfo.length);
        if (_targetToAttack != null)
        {
            _targetToAttack.GetComponent<IHealth>().DoDamage(_damage);
        }
        isAttacking = false;
    }

    private bool isEnemyPossibleToAttack()
    {
        if (_movement.EnemyPosition != null)
        {
            _targetToAttack = _movement.EnemyPosition.gameObject;
            float distanceToEnemy = Vector2.Distance(transform.position, _targetToAttack.transform.position);
            if (_targetToAttack != null && _collider.IsTouching(_targetToAttack.GetComponent<Collider2D>()) || distanceToEnemy <= _attackRagne)
            {
                return true;
            }
        }
        return false;
    }

}
