﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum Requestor
{
    Player,
    AI,
}

public class MinionFactory : MonoBehaviour
{

    public List<GameObject> MinionList;
    public int copiesOfMinion = 4;

    private List<GameObject> _playerMinionInstances;
    private List<GameObject> _aIMinionInstances;

    public void Awake()
    {
        Transform eyes;
        _playerMinionInstances = new List<GameObject>();
        _aIMinionInstances = new List<GameObject>();

        foreach (GameObject minion in MinionList)
        {
            for (int i = 0; i < copiesOfMinion; i++)
            {
                GameObject playerMinion = (GameObject)Instantiate(minion);
                playerMinion.AddComponent<PlayerMovement>();
                playerMinion.tag = "Player";
                eyes = playerMinion.transform.GetChild(0);
                eyes.gameObject.tag = "Player";
                playerMinion.SetActive(false);
                _playerMinionInstances.Add(playerMinion);

                GameObject AIMinion = (GameObject)Instantiate(minion);
                AIMinion.AddComponent<AIMovement>();
                AIMinion.tag = "AI";
                eyes = AIMinion.transform.GetChild(0);
                eyes.gameObject.tag = "AI";
                AIMinion.SetActive(false);
                _aIMinionInstances.Add(AIMinion);
            }
        }
    }

    public GameObject GetMinion(MinionEnum selectedMinion, Requestor requestor)
    {
        List<GameObject> tempMinionList;
        if (requestor == Requestor.Player)
        {
            tempMinionList = _playerMinionInstances;
        }
        else
        {
            tempMinionList = _aIMinionInstances;
        }
        foreach (GameObject minion in tempMinionList)
        {
            if (minion.GetComponent<MinionParameters>().MinionName == selectedMinion)
            {
                if (!minion.activeInHierarchy)
                {
                    return minion;
                }
            }
        }
        return null;
    }


}
