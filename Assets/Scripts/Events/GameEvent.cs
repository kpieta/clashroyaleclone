﻿using UnityEngine;
using System.Collections;

public class GameEvent
{

    public delegate void MinionEventHandler(MinionEnum minion);
    public delegate void DeckEventHandler();
    public delegate void LevelHandler(GameObject sender);
    public delegate void ManaHandler(int manaAmmount);
    public delegate void SendingInfoHandler(string message);
    public delegate void GoldHandler(int goldLeft);

    public static event MinionEventHandler MinionSpawned;
    public static event MinionEventHandler MinionSelected;
    public static event DeckEventHandler DeckUpdated;
    public static event DeckEventHandler BoughtNewMinion;
    public static event LevelHandler TowerDestroyed;
    public static event ManaHandler ManaUpdated;
    public static event SendingInfoHandler SendInfoOnScreen;
    public static event GoldHandler GoldSpend;


    #region Events related to battle
    public static void OnManaUpdated(int manaAmmount)
    {
        ManaHandler manaUpdated = ManaUpdated;
        if (manaUpdated != null)
        {
            manaUpdated(manaAmmount);
        }
    }

    public static void OnMinionSpawned(MinionEnum spawnedMinion)
    {
        MinionEventHandler minionSpawned = MinionSpawned;
        if (minionSpawned != null)
        {
            minionSpawned(spawnedMinion);
        }
    }

       public static void OnTowerDestroyed(GameObject sender)
    {
        LevelHandler levelCompleted = TowerDestroyed;
        if (levelCompleted != null)
        {
            levelCompleted(sender);
        }
    }

    public static void OnMinionSelected(MinionEnum selectedMinion)
    {
        MinionEventHandler minionSelected = MinionSelected;
        if (minionSelected != null)
        {
            minionSelected(selectedMinion);
        }
    }
    #endregion

    #region Events related to shop
    public static void OnGoldSpend(int goldLeft)
    {
        GoldHandler goldSpend = GoldSpend;
        if (goldSpend != null)
        {
            goldSpend(goldLeft);
        }
    }

    public static void OnBouthtNewMinion()
    {
        DeckEventHandler boughtNewMinion = BoughtNewMinion;
        if (boughtNewMinion != null)
        {
            boughtNewMinion();
        }
    }
    #endregion

    #region Events related to battle and shop
    public static void OnSendInfoOnScreen(string message)
    {
        SendingInfoHandler sendInfoOnScreen = SendInfoOnScreen;
        if (sendInfoOnScreen != null)
        {
            sendInfoOnScreen(message);
        }
    }

    public static void OnHandDeckUpdated()
    {
        DeckEventHandler handdeckUpdated = DeckUpdated;
        if (handdeckUpdated != null)
        {
            handdeckUpdated();
        }
    }

    public static void OnDeckUpdated()
    {
        DeckEventHandler deckUpdated = DeckUpdated;
        if (deckUpdated != null)
        {
            deckUpdated();
        }
    }
    #endregion
















 
}
