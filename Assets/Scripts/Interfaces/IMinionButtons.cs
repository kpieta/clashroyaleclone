﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public interface IMinionButtons
{
    MinionEnum Minion { get; }


    void AttachMinionToButton(MinionEnum minion);
    void RefreshIcon();
}
