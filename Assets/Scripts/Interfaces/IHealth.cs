﻿using UnityEngine;
using System.Collections;

public interface IHealth
{
    int Health { get; }

    void DoDamage(int damage);

}
