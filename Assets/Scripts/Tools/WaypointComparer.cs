﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum StartingPosition
{
    Bottom,
    Left,
    Top,
    Right,
}

public enum Path
{
    Left,
    Right,
}

public class WaypointComparer : IComparer<Transform>
{
    public StartingPosition startingPosition { get; private set; }
    public Path pathPosition { get; private set; }

    public void SetStartingPosition(StartingPosition startingPosition)
    {
        this.startingPosition = startingPosition;
    }

    public void SetPathPosition(Path pathPosition)
    {
        this.pathPosition = pathPosition;
    }

    public int Compare(Transform x, Transform y)
    {
        if (startingPosition == StartingPosition.Bottom)
        {
            if (x.transform.position.y < y.transform.position.y)
            {
                return -1;
            }
            else if (x.transform.position.y > y.transform.position.y)
            {
                return 1;
            }
            else
            {
                if (pathPosition == Path.Left)
                {
                    if (x.transform.position.x < y.transform.position.x)
                    {
                        return -1;
                    }
                    else if (x.transform.position.x > y.transform.position.x)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }

                }
                else
                {
                    if (x.transform.position.x > y.transform.position.x)
                    {
                        return -1;
                    }
                    else if (x.transform.position.x < y.transform.position.x)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }

        else if (startingPosition == StartingPosition.Left)
        {

            if (x.transform.position.x < y.transform.position.x)
            {
                return -1;
            }
            else if (x.transform.position.x > y.transform.position.x)
            {
                return 1;
            }
            else
            {
                if (pathPosition == Path.Left)
                {
                    if (x.transform.position.y > y.transform.position.y)
                    {
                        return -1;
                    }
                    else if (x.transform.position.y < y.transform.position.y)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    if (x.transform.position.y < y.transform.position.y)
                    {
                        return -1;
                    }
                    else if (x.transform.position.y > y.transform.position.y)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }
        else if (startingPosition == StartingPosition.Top)
        {
            if (x.transform.position.y > y.transform.position.y)
            {
                return -1;
            }
            else if (x.transform.position.y < y.transform.position.y)
            {
                return 1;
            }
            else
            {
                if (pathPosition == Path.Left)
                {
                    if (x.transform.position.x < y.transform.position.x)
                    {
                        return -1;
                    }
                    else if (x.transform.position.x > y.transform.position.x)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    if (x.transform.position.x > y.transform.position.x)
                    {
                        return -1;
                    }
                    else if (x.transform.position.x < y.transform.position.x)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }

        else
        {
            if (x.transform.position.x > y.transform.position.x)
            {
                return -1;
            }
            else if (x.transform.position.x < y.transform.position.x)
            {
                return 1;
            }
            else
            {
                if (pathPosition == Path.Left)
                {
                    if (x.transform.position.y > y.transform.position.y)
                    {
                        return -1;
                    }
                    else if (x.transform.position.y < y.transform.position.y)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    if (x.transform.position.y < y.transform.position.y)
                    {
                        return -1;
                    }
                    else if (x.transform.position.y > y.transform.position.y)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }
    }
}
