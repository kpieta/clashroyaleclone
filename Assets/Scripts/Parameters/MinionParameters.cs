﻿using UnityEngine;
using System.Collections;
using System;

public class MinionParameters : MonoBehaviour, IHealth
{

    public MinionEnum MinionName;
    public float MovementSpeed;
    public float RotationSpeed;
    public float PushForce;
    public float AttackRange;
    public int GoldPerKill;
    public int ManaCost;
    public int Damage;
    public int AttackSpeed;
    public int SightRange = 20;

    [SerializeField]
    private int _health;

    public int Health
    {
        get
        {
            return _health;
        }
    }

    public void DoDamage(int damage)
    {
        _health -= damage;
        if (_health <= 0)
        {
            Manager.GameManager.GatherGold(GoldPerKill);
            gameObject.SetActive(false);
        }
    }
}
