﻿using UnityEngine;
using System.Collections;
using System;

public class BuildingParameters : MonoBehaviour, IHealth
{

    public int GoldPerKill;

    [SerializeField]
    private int _health;


    public int Health
    {
        get
        {
            return _health;
        }
    }

    public void DoDamage(int damage)
    {
        _health -= damage;
        if (_health <= 0)
        {

            Manager.GameManager.GatherGold(GoldPerKill);
            Destroy(gameObject);
        }
    }
}
